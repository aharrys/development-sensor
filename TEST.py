import paho.mqtt.client as mqtt
import json
import datetime
import requests
import time
import sys
import os
import subprocess
import signal

#-----------------Looping----------------------
looping = 0
timeloop = 50

count_minutes = 0

#class Timeout(Exception):
#    pass
#def handler(signum, frame):
#    raise Timeout
    
#-------------------------GLOBAL----------------
WLD_status = '0'

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("zigbee2mqtt/0x00158d000651823f") # topic you want to subscribe
    print("Connected with result code "+str(rc))
    

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    global WLD_status
    print(msg.topic+" "+str(msg.payload)) # displaying the data on console
    node_data = str(msg.payload)
    my_dict = json.loads(msg.payload.decode("utf-8"))
    battery = my_dict.get('battery')
    water_leak = my_dict.get('water_leak')
    now = datetime.datetime.now()
    print(now.strftime('%Y-%m-%d %H:%M:%S'))
    print("battery:" +str(battery))
    print("water_leak:" +str(water_leak))
    if (water_leak == True):
        WLD_status = '1'
        #send data to DBIOT
    else:
        WLD_status = '0'
        #send data to DBIOT
    print("WLD_Status:" +str(WLD_status))


def main():
    now = datetime.datetime.now()
    print(now.strftime('%Y-%m-%d %H:%M:%S'))
    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message
    client.connect("localhost", 1883, 60) # your mqtt host and port
    client.loop_forever()
    print('')
    
if __name__ == '__main__':
    while True:
        looping += 1
        if looping > timeloop:
            {
            #os.system('sudo reboot now')
            }
        now = datetime.datetime.now()
        print(now.strftime('%Y-%m-%d %H:%M:%S'))
        client = mqtt.Client()
        client.on_connect = on_connect
        client.on_message = on_message
        client.connect("localhost", 1883, 60) # your mqtt host and port
        count_minutes += 1
        client.loop_start()
        time.sleep(1)
        if count_minutes > 10:
            print ("one minutes Reset")
            #send data to DBIOT
            print ("WLD_Status:" +str(WLD_status))
            count_minutes = 0
        print (count_minutes)
        client.loop_stop()
        #print("WLD_Status:" +str(WLD_status))
            